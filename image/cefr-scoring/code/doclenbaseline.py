#Adding a document length baseline for final version.
import os
import collections
import numpy as np
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor, RandomForestClassifier, RandomForestRegressor
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score,cross_val_predict,StratifiedKFold 
from sklearn.metrics import f1_score,classification_report,accuracy_score,confusion_matrix, mean_absolute_error
from sklearn.svm import LinearSVC

seed = None

def getdoclen(conllufilepath):
    fh =  open(conllufilepath, encoding="utf-8")
    allText = []
    sent_id = 0
    for line in fh:
        if line == "\n":
            sent_id = sent_id+1
        elif not line.startswith("#") and line.split("\t")[3] != "PUNCT":
            word = line.split("\t")[1]
            allText.append(word)
    fh.close()
    return len(allText)

def getfeatures(dirpath):
    files = os.listdir(dirpath)
    cats = []
    doclenfeaturelist = []
    for filename in files:
        if filename.endswith(".txt"):
            doclenfeaturelist.append([getdoclen(os.path.join(dirpath,filename))])
            cats.append(filename.split(".txt")[0].split("_")[-1])
    return doclenfeaturelist,cats

def singleLangClassificationWithoutVectorizer(train_vector,train_labels): #test_vector,test_labels):
    k_fold = StratifiedKFold(10,random_state=seed)
    classifiers = [RandomForestClassifier(class_weight="balanced",n_estimators=300,random_state=seed), LinearSVC(class_weight="balanced",random_state=seed), LogisticRegression(class_weight="balanced",random_state=seed)] #Add more later
    #classifiers = [MLPClassifier(max_iter=500)]
    #RandomForestClassifer(), GradientBoostClassifier()
    #Not useful: SVC with kernels - poly, sigmoid, rbf.
    f1w_best, f1m_best, m_best = 0, 0, None
    for classifier in classifiers:
        print(classifier)
        cross_val = cross_val_score(classifier, train_vector, train_labels, cv=k_fold, n_jobs=1)
        predicted = cross_val_predict(classifier, train_vector, train_labels, cv=k_fold)
        print(cross_val)
        print(sum(cross_val)/float(len(cross_val)))
        print(confusion_matrix(train_labels, predicted))
        f1w = f1_score(train_labels,predicted,average='weighted')
        f1m = f1_score(train_labels,predicted,average='macro')
        print(f1m)
        if f1w > f1w_best:
            f1w_best = f1w
            f1m_best = f1m
            m_best = type(classifier).__name__
    return f1w_best, f1m_best, m_best

def crossLangClassificationWithoutVectorizer(train_vector, train_labels, test_vector, test_labels):
    classifiers = [RandomForestClassifier(class_weight="balanced",n_estimators=300,random_state=seed), LinearSVC(class_weight="balanced",random_state=seed), LogisticRegression(class_weight="balanced",random_state=seed)]
    f1w_best, f1m_best, m_best = 0, 0, None
    for classifier in classifiers:
        classifier.fit(train_vector,train_labels)
        predicted = classifier.predict(test_vector)
        print(np.mean(predicted == test_labels,dtype=float))
        print(confusion_matrix(test_labels,predicted))
        f1w = f1_score(test_labels,predicted,average='weighted')
        f1m = f1_score(test_labels,predicted,average='macro')
        print(f1w)
        if f1w > f1w_best:
            f1w_best = f1w
            f1m_best = f1m
            m_best = type(classifier).__name__
    return f1w_best, f1m_best, m_best


def main():
    itdirpath = "../Datasets/IT-Parsed"
    dedirpath = "../Datasets//DE-Parsed"
    czdirpath = "../Datasets/CZ-Parsed"
    print("************DE baseline:****************")
    defeats,delabels = getfeatures(dedirpath)
    f1w, f1m, clf = singleLangClassificationWithoutVectorizer(defeats,delabels)
    print("RESULTS\tbase\t{}\t{}\t{}\t{}".format("de", 100*f1w, 100*f1m, clf))
    print("************IT baseline:****************")
    itfeats,itlabels = getfeatures(itdirpath)
    f1w, f1m, clf = singleLangClassificationWithoutVectorizer(itfeats,itlabels)
    print("RESULTS\tbase\t{}\t{}\t{}\t{}".format("it", 100*f1w, 100*f1m, clf))
    print("************CZ baseline:****************")
    czfeats,czlabels = getfeatures(czdirpath)
    f1w, f1m, clf = singleLangClassificationWithoutVectorizer(czfeats,czlabels)
    print("RESULTS\tbase\t{}\t{}\t{}\t{}".format("cz", 100*f1w, 100*f1m, clf))

    print("*** Train with DE, test with IT baseline******")
    f1w, f1m, clf = crossLangClassificationWithoutVectorizer(defeats,delabels, itfeats,itlabels)
    print("RESULTS\tbase\t{}\t{}\t{}\t{}".format("de->it", 100*f1w, 100*f1m, clf))
     
    print("*** Train with DE, test with CZ baseline ******")
    f1w, f1m, clf = crossLangClassificationWithoutVectorizer(defeats,delabels, czfeats,czlabels)
    print("RESULTS\tbase\t{}\t{}\t{}\t{}".format("de->cz", 100*f1w, 100*f1m, clf))
    
    bigfeats = []
    bigcats = []
    bigfeats.extend(defeats)
    bigfeats.extend(itfeats)
    bigfeats.extend(czfeats)
    bigcats.extend(delabels)
    bigcats.extend(itlabels)
    bigcats.extend(czlabels)
    print("****Multilingual classification baseline*************")
    f12, f1m, clf = singleLangClassificationWithoutVectorizer(bigfeats,bigcats)
    print("RESULTS\tbase\t{}\t{}\t{}\t{}".format("mega", 100*f1w, 100*f1m, clf))
    
main()

