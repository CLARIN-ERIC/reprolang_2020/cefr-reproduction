#!/bin/bash

TMPDIR=/tmp/cefr-scoring
cd cefr-scoring/code
mkdir -p $TMPDIR

echo This will take some time...

echo Calculating baseline scores...
for i in {0..9};do 
    python3 ./doclenbaseline.py > $TMPDIR/baseline-$i.out  2>$TMPDIR/baseline-errors
done

echo Calculating monolingual embedding scores...
for i in {0..9};do
    (for l in CZ DE IT;do
        python3 ./monolingual_cv.py ../Datasets/$l-Parsed;
    done) > $TMPDIR/monolingual_cv-$i.out 2>$TMPDIR/monolingual_cv-errors
done

echo Calculating multilingual embedding scores...
for i in {0..9};do 
    python3 ./multi_lingual.py ../Datasets/ > $TMPDIR/multi_lingual-$i.out 2>$TMPDIR/multi_lingual-errors
done

echo Calculating multilingual embedding scores without language information...
for i in {0..9};do 
    python3 ./multi_lingual_no_langfeat.py../Datasets/ > $TMPDIR/multi_lingual_no_langfeat-$i.out 2>$TMPDIR/multi_lingual_no_langfeat-errors
done

echo Calculating scores based on n-gram and domain features.
for i in {0..9};do 
    python3 ./IdeaPOC.py > $TMPDIR/ideapoc-$i.out 2>$TMPDIR/ideapoc-errors
done

cd /
python3 /make-tables.py -o /output/tables_and_plots $TMPDIR/*.out
